# Gitkube

`git push` to a Kubernetes cluster.

- [Gitkube site](https://gitkube.sh/)
- [GitHub repository](https://github.com/hasura/gitkube)

- [ ] Install and set up Gitkube on a new Wedos VPS
- [ ] Follow the [example by Hasura](https://github.com/hasura/gitkube-example)
